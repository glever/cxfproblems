package be.glever.bosawsdl;

import be.belgium.fsb.header.FsbHeaderType;
import be.belgium.fsb.searchpersonservice.messages.v3_00.SearchPersonByAddressRequestType;
import be.belgium.fsb.searchpersonservice.v3_00.WsPersonSearchService;
import be.belgium.fsb.searchpersonservice.v3_00.WsPersonSearchServicePortType;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;


public class SoapTest {

	@Test
	public void invokeFakeEndpoint() {
		WireMockServer wireMockServer = new WireMockServer(wireMockConfig().dynamicPort());
		wireMockServer.start();
		WireMock.configureFor("localhost", wireMockServer.port());

		stubFor(WireMock.any(WireMock.anyUrl()).willReturn(aResponse().withBody("z")));


		WsPersonSearchService service = new WsPersonSearchService();
		WsPersonSearchServicePortType port = service.getWsPersonSearchServicePort();
		((BindingProvider)port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://localhost:" + wireMockServer.port() );

		Holder<FsbHeaderType> responseHolder = new Holder<FsbHeaderType>();
		String sentOnBehalfOf = "sentOnBehalfOf";
		FsbHeaderType header = new FsbHeaderType();
		SearchPersonByAddressRequestType request = new SearchPersonByAddressRequestType();

		port.searchPersonByAddress(request, header, sentOnBehalfOf, responseHolder);
	}
}
