<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:per="http://fsb.belgium.be/data/entity/person/v2_00" xmlns:nam="http://fsb.belgium.be/data/entity/person/name/v1_00" xmlns:nat="http://fsb.belgium.be/data/entity/person/nationality/v1_00" xmlns:num="http://fsb.belgium.be/data/entity/person/number/v1_00" xmlns:bir="http://fsb.belgium.be/data/entity/person/birth/v1_00" xmlns:dec="http://fsb.belgium.be/data/entity/person/decease/v1_00" xmlns:civ="http://fsb.belgium.be/data/entity/person/civilstate/v2_00" xmlns:leg="http://fsb.belgium.be/data/entity/person/legalcohabitation/v1_00" xmlns:dis="http://fsb.belgium.be/data/entity/person/legaldisability/v1_00" xmlns:pro="http://fsb.belgium.be/data/entity/person/profession/v1_00" xmlns:sha="http://fsb.belgium.be/data/entity/person/sharedhosting/v1_00" xmlns:dat="http://fsb.belgium.be/data/common/date/v2_00" xmlns:cod="http://fsb.belgium.be/data/common/code/v1_00" xmlns:lab="http://fsb.belgium.be/data/common/label/v1_00" xmlns:cvmap="http://data.europa.eu/core-vocabularies/" targetNamespace="http://fsb.belgium.be/data/entity/person/v2_00" elementFormDefault="qualified" attributeFormDefault="unqualified" version="2.00">
	<xs:annotation>
		<xs:documentation>
			Definitions of person types.
			
			Updates january 2017: added legalCohabitation, legalDisabilty, legalGuardian and personRegister to extendedPerson
								added sharedHosting
								added link to new civil state xsd and new date xsd
								added previousPersonNumber and declaredBirthTime and declaredDeceaseTime
								added lastUpdatedDate
		</xs:documentation>
	</xs:annotation>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/name/v1_00" schemaLocation="../name/v1_00/name.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/number/v1_00" schemaLocation="../number/v1_00/personNumber.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/common/date/v2_00" schemaLocation="../../../common/date/v2_00/date.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/common/code/v1_00" schemaLocation="../../../common/code/v1_00/code.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/common/label/v1_00" schemaLocation="../../../common/label/v1_00/labels.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/birth/v1_00" schemaLocation="../birth/v1_00/birth.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/decease/v1_00" schemaLocation="../decease/v1_00/decease.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/civilstate/v2_00" schemaLocation="../civilstate/v2_00/civilState.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/legalcohabitation/v1_00" schemaLocation="../legalcohabitation/v1_00/legalCohabitation.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/legaldisability/v1_00" schemaLocation="../legaldisability/v1_00/legalDisability.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/profession/v1_00" schemaLocation="../profession/v1_00/profession.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/nationality/v1_00" schemaLocation="../nationality/v1_00/nationality.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/sharedhosting/v1_00" schemaLocation="../sharedhosting/v1_00/sharedHosting.xsd"/>
	<xs:element name="personNumber" type="num:PersonNumberType" nillable="true">
		<xs:annotation>
			<xs:documentation>
				<cvmap:Mapping>
					<cvmap:URI>http://fsb.belgium.be/data/entity/person:personNumber</cvmap:URI>
					<cvmap:Label>personNumber</cvmap:Label>
					<cvmap:Definition>
							A person number is a unique identifier for a person. In Belgium, the person number is defined by the birth date in the format yymmdd plus a three-digit sequential number, and a two-digit control number. Odd or even sequential numbers also indicate the gender of the person. The length of the person number is 11 digits
							</cvmap:Definition>
					<cvmap:CoreVocURI>http://data.europa.eu/core-vocabularies/PersonIdentifier</cvmap:CoreVocURI>
					<cvmap:CoreVocVersion>1.0</cvmap:CoreVocVersion>
					<cvmap:MappingRelation>exact</cvmap:MappingRelation>
					<cvmap:MappingComment/>
				</cvmap:Mapping>
			</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="name" type="nam:NameType" nillable="true"/>
	<xs:element name="gender" type="cod:CodeType" nillable="true">
		<xs:annotation>
			<xs:documentation>
				<cvmap:Mapping>
					<cvmap:URI>http://fsb.belgium.be/data/entity/person:gender.code</cvmap:URI>
					<cvmap:Label>gender</cvmap:Label>
					<cvmap:Definition>
							ISO-5218 or nil (empty gender element)
					</cvmap:Definition>
					<cvmap:CoreVocURI>http://data.europa.eu/core-vocabularies/PersonGender</cvmap:CoreVocURI>
					<cvmap:CoreVocVersion>1.0</cvmap:CoreVocVersion>
					<cvmap:MappingRelation>exact</cvmap:MappingRelation>
					<cvmap:MappingComment/>
				</cvmap:Mapping>
			</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="nationality" type="cod:CodeType" nillable="true">
		<xs:annotation>
			<xs:documentation>
				<cvmap:Mapping>
					<cvmap:URI>http://fsb.belgium.be/data/entity/person:nationality.code</cvmap:URI>
					<cvmap:Label>nationality</cvmap:Label>
					<cvmap:Definition>ISO-3166 (alpha 2)</cvmap:Definition>
					<cvmap:CoreVocURI>http://data.europa.eu/core-vocabularies/PersonCitizenship</cvmap:CoreVocURI>
					<cvmap:CoreVocVersion>1.0</cvmap:CoreVocVersion>
					<cvmap:MappingRelation>narrow</cvmap:MappingRelation>
					<cvmap:MappingComment/>
				</cvmap:Mapping>
			</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="birthPlace" type="bir:BirthPlaceType" nillable="true"/>
	<xs:element name="officialBirthDate" type="dat:DateType" nillable="true">
		<xs:annotation>
			<xs:documentation>
				<cvmap:Mapping>
					<cvmap:URI>http://fsb.belgium.be/data/entity/person:officialBirthDate</cvmap:URI>
					<cvmap:Label>officialBirthDate</cvmap:Label>
					<cvmap:Definition>Official birth date</cvmap:Definition>
					<cvmap:CoreVocURI>http://data.europa.eu/core-vocabularies/PersonDateOfBirth</cvmap:CoreVocURI>
					<cvmap:CoreVocVersion>1.0</cvmap:CoreVocVersion>
					<cvmap:MappingRelation>exact</cvmap:MappingRelation>
					<cvmap:MappingComment/>
				</cvmap:Mapping>
			</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="deceasePlace" type="dec:DeceasePlaceType" nillable="true"/>
	<xs:element name="officialDeceaseDate" type="dat:DateType" nillable="true">
		<xs:annotation>
			<xs:documentation>
				<cvmap:Mapping>
					<cvmap:URI>http://fsb.belgium.be/data/entity/person:officialDeceaseDate</cvmap:URI>
					<cvmap:Label>officialDeceaseDate</cvmap:Label>
					<cvmap:Definition>Official decease date</cvmap:Definition>
					<cvmap:CoreVocURI>http://data.europa.eu/core-vocabularies/PersonDateOfDeath</cvmap:CoreVocURI>
					<cvmap:CoreVocVersion>1.0</cvmap:CoreVocVersion>
					<cvmap:MappingRelation>exact</cvmap:MappingRelation>
					<cvmap:MappingComment/>
				</cvmap:Mapping>
			</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="civilState" type="cod:CodeType" nillable="true"/>
	<xs:complexType name="PersonType" abstract="true">
		<xs:annotation>
			<xs:documentation>
				The base class for person types. The base person must contain a person number and a name.
				If, for whatever reason, these do not exist or cannot be filled in, they must explicitly
				be set to nil.
				The base class is abstract and no non-abstract elements can exist with this type. Its sole
				purpose is to define the mandatory fields of a person. Instances must use any of the subtypes.
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element ref="per:personNumber"/>
			<xs:element ref="per:name"/>
		</xs:sequence>
		<xs:attribute name="trust" type="cod:TrustType" use="optional"/>
		<xs:attribute name="source" type="cod:CodeSourceType" use="optional"/>
	</xs:complexType>
	<xs:complexType name="BasicPersonType">
		<xs:annotation>
			<xs:documentation>
				A simple person is a person holding basic information, such as the person number, the names,
				the gender, the nationality, the birth place, the official birth date and the official decease 
				date. Most of these fields are optional, but should be filled in, if known. If not known, the
				optional fields should be omitted. If known, but on purpose not propagated, they should be set 
				to nil.
				For most purposes, the simple person holds enough information. If not, the detailed person
				whould be considered instead.
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="per:PersonType">
				<xs:sequence>
					<xs:element ref="per:gender"/>
					<xs:element ref="per:nationality" minOccurs="0" maxOccurs="unbounded"/>
					<xs:element ref="per:birthPlace" minOccurs="0"/>
					<xs:element ref="per:officialBirthDate" minOccurs="0"/>
					<xs:element ref="per:deceasePlace" minOccurs="0"/>
					<xs:element ref="per:officialDeceaseDate" minOccurs="0"/>
					<xs:element ref="per:civilState" minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="ExtendedPersonType">
		<xs:annotation>
			<xs:documentation>
				A detailed person is a simple person, with additional, optional information. 
				The detailed person is modelled after the information present in the Belgian National Register.
				Information that is not relevant to certain applications/services may be omitted.
				All fields are nillable, but fields should only be set to nil if the information is deliberately
				witheld.
				
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="per:BasicPersonType">
				<xs:sequence>
					<xs:element name="previousPersonNumber" type="num:PersonNumberType" nillable="true" minOccurs="0"/>
					<xs:element name="declaredBirthDate" type="dat:DateType" nillable="true" minOccurs="0"/>
					<xs:element name="officialBirthTime" type="dat:TimeType" nillable="true" minOccurs="0"/>
					<xs:element name="declaredDeceaseDate" type="dat:DateType" nillable="true" minOccurs="0"/>
					<xs:element name="officialDeceaseTime" type="dat:TimeType" nillable="true" minOccurs="0"/>
					<xs:element name="birthDocuments" type="bir:BirthDocumentsType" nillable="true" minOccurs="0"/>
					<xs:element name="deceaseDocuments" type="dec:DeceaseDocumentsType" nillable="true" minOccurs="0"/>
					<xs:element name="nobilityTitle" type="nam:NobilityTitleType" nillable="true" minOccurs="0" maxOccurs="unbounded"/>
					<xs:element name="pseudonym" type="nam:PseudonymType" nillable="true" minOccurs="0" maxOccurs="unbounded"/>
					<xs:element name="disappearanceDate" type="dat:DateType" nillable="true" minOccurs="0"/>
					<xs:element name="disappearanceComment" type="lab:CommentType" nillable="true" minOccurs="0"/>
					<xs:element name="civilStateInformation" type="civ:CivilStateInformationType" nillable="true" minOccurs="0"/>
					<xs:element name="legalCohabitationInformation" type="leg:LegalCohabitationInformationType" nillable="true" minOccurs="0"/>
					<xs:element name="legalDisabilityInformation" type="dis:LegalDisabilityInformationType" nillable="true" minOccurs="0"/>
					<xs:element name="legalGuardianInformation" type="dis:LegalGuardianInformationType" nillable="true" minOccurs="0"/>
					<xs:element name="profession" type="pro:ProfessionType" nillable="true" minOccurs="0" maxOccurs="unbounded"/>
					<xs:element name="nationalityReason" type="nat:NationalityReasonType" minOccurs="0"/>
					<xs:element name="personRegister" type="cod:CodeType" nillable="true" minOccurs="0"/>
					<xs:element name="sharedHosting" type="sha:SharedHostingType" nillable="true" minOccurs="0"/>
					<xs:element name="lastUpdatedDate" type="dat:DateType" nillable="true" minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="basicPerson" type="per:BasicPersonType">
		<xs:annotation>
			<xs:documentation>
				Root element holding a basic person.
			</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="extendedPerson" type="per:ExtendedPersonType">
		<xs:annotation>
			<xs:documentation>
				Root element holding a detailed person.
			</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="person">
		<xs:annotation>
			<xs:documentation>
				<cvmap:Mapping>
					<cvmap:URI>http://fsb.belgium.be/data/entity/person:person</cvmap:URI>
					<cvmap:Label>person</cvmap:Label>
					<cvmap:Definition>The base class for person types. The base person must contain a person number and a name.</cvmap:Definition>
					<cvmap:CoreVocURI>http://data.europa.eu/core-vocabularies/Person</cvmap:CoreVocURI>
					<cvmap:CoreVocVersion>1.0</cvmap:CoreVocVersion>
					<cvmap:MappingRelation>exact</cvmap:MappingRelation>
					<cvmap:MappingComment/>
				</cvmap:Mapping>
			</xs:documentation>
			<!-- 
			Because not all XML web service frameworks correctly interprete substitution groups, it was decided
			to work with XML choices instead of substitution groups. This means that you will always have an element
			'person' which contains a subelement 'simplePerson' or 'detailedPerson.'
			Using substitution groups would be simpler and yields cleaner XML; it also more extensible and looser
			coupled than choice groups. Hence, it is advisable to change the choice-group constructions in the
			future and to prefer substitution groups.
			-->
		</xs:annotation>
		<xs:complexType>
			<xs:choice>
				<xs:element ref="per:basicPerson"/>
				<xs:element ref="per:extendedPerson"/>
			</xs:choice>
		</xs:complexType>
	</xs:element>
</xs:schema>
