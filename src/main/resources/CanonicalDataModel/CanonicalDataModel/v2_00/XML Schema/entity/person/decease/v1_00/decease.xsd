<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:dec="http://fsb.belgium.be/data/entity/person/decease/v1_00" xmlns:cod="http://fsb.belgium.be/data/common/code/v1_00" xmlns:lab="http://fsb.belgium.be/data/common/label/v1_00" xmlns:cvmap="http://data.europa.eu/core-vocabularies/" targetNamespace="http://fsb.belgium.be/data/entity/person/decease/v1_00" elementFormDefault="qualified" attributeFormDefault="unqualified" version="1.00">
	<xs:annotation>
		<xs:documentation>
			Definitions of decease-related types.
			05/01/2017: added documentation about EU core vocabulary mappings
		</xs:documentation>
	</xs:annotation>
	<xs:import namespace="http://fsb.belgium.be/data/common/code/v1_00" schemaLocation="../../../../common/code/v1_00/code.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/common/label/v1_00" schemaLocation="../../../../common/label/v1_00/labels.xsd"/>
	<xs:complexType name="DeceasePlaceType">
		<xs:annotation>
			<xs:documentation>
				A decease place is generally defined as a municipality and a country.
				
				For Belgian citizens, the municipality is generally known, and should be filled in accordingly.
				The country should be set to Belgium in this case.
				
				For foreigners, the municipality is not always known, but at least the country should be filled in.
				
				If the municipality is not known, it is ommitted.
				
				If the country is not known, for whatever reason, it should be explicitly set to nill. This should be
				an exceptional case.
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="municipality" type="cod:CodeType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					<cvmap:Mapping>
							<cvmap:URI>http://fsb.belgium.be/data/entity/person/decease:deceasePlace.municipality</cvmap:URI>
							<cvmap:Label>deceasePlaceMunicipality</cvmap:Label>
							<cvmap:Definition>
							The municipality is defined as a code type, which holds a simple code and a description.
							For Belgian municipalities, the simple code will be the INS/NIS code, and language-dependent
							descriptions providing the name of the municipality may be added.
							For foreign municipalities, the simple code will generally not be defined, and it is set
							to nill.
							</cvmap:Definition>
							<cvmap:CoreVocURI>http://data.europa.eu/core-vocabularies/PersonPlaceOfDeath</cvmap:CoreVocURI>
							<cvmap:CoreVocVersion>1.0</cvmap:CoreVocVersion>
							<cvmap:MappingRelation>exact</cvmap:MappingRelation>
							<cvmap:MappingComment/>
						</cvmap:Mapping>
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="country" type="cod:CodeType" nillable="true">
				<xs:annotation>
					<xs:documentation>
					<cvmap:Mapping>
							<cvmap:URI>http://fsb.belgium.be/data/entity/person/decease:deceasePlace.country</cvmap:URI>
							<cvmap:Label>deceasePlaceCountry</cvmap:Label>
							<cvmap:Definition>
							The country is defined as a code type, which holds a simple code and a description.
							In general, the ISO country code is used as the simple code, and language-dependent
							descriptions providing the name of the country may be added.
							</cvmap:Definition>
							<cvmap:CoreVocURI>http://data.europa.eu/core-vocabularies/PersonCountryOfDeath</cvmap:CoreVocURI>
							<cvmap:CoreVocVersion>1.0</cvmap:CoreVocVersion>
							<cvmap:MappingRelation>exact</cvmap:MappingRelation>
							<cvmap:MappingComment/>
						</cvmap:Mapping>
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="comment" type="lab:CommentType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="DeceaseActType">
		<xs:annotation>
			<xs:documentation>
				A decease act contains a number, plus an identifier defining the suppletory register where
				the act is held.
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="actNumber" type="xs:string" nillable="true"/>
			<xs:element name="suppletoryRegistery" type="xs:string" nillable="true" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="DeceaseDocumentsType">
		<xs:annotation>
			<xs:documentation>
				Container for decease-related documents. For documents that do not exist, the corresponding element
				is simply omitted.
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="deceaseAct" type="dec:DeceaseActType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
</xs:schema>
