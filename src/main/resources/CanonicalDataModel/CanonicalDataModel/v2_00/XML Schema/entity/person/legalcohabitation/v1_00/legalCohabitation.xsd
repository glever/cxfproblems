<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:leg="http://fsb.belgium.be/data/entity/person/legalcohabitation/v1_00" xmlns:cod="http://fsb.belgium.be/data/common/code/v1_00" xmlns:lab="http://fsb.belgium.be/data/common/label/v1_00" xmlns:dat="http://fsb.belgium.be/data/common/date/v2_00" xmlns:nam="http://fsb.belgium.be/data/entity/person/name/v1_00" xmlns:num="http://fsb.belgium.be/data/entity/person/number/v1_00" targetNamespace="http://fsb.belgium.be/data/entity/person/legalcohabitation/v1_00" elementFormDefault="qualified" attributeFormDefault="unqualified" version="1.00">
	<xs:annotation>
		<xs:documentation>
			Definitions of legal-cohabitation-related types.
		</xs:documentation>
	</xs:annotation>
	<xs:import namespace="http://fsb.belgium.be/data/common/code/v1_00" schemaLocation="../../../../common/code/v1_00/code.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/common/label/v1_00" schemaLocation="../../../../common/label/v1_00/labels.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/common/date/v2_00" schemaLocation="../../../../common/date/v2_00/date.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/name/v1_00" schemaLocation="../../name/v1_00/name.xsd"/>
	<xs:import namespace="http://fsb.belgium.be/data/entity/person/number/v1_00" schemaLocation="../../number/v1_00/personNumber.xsd"/>
	<xs:complexType name="LegalCohabitationPartnerType">
		<xs:annotation>
			<xs:documentation>
				The identification of the legal cohabitation partner
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="number" type="num:PersonNumberType" nillable="true" minOccurs="0"/>
			<xs:element name="name" type="nam:NameType" nillable="true" minOccurs="0"/>
			<xs:element name="birthDate" type="dat:DateType" nillable="true" minOccurs="0"/>
			<xs:element name="birthPlace" type="leg:PartnerBirthPlaceType" nillable="true" minOccurs="0"/>
			<xs:element name="gender" type="cod:CodeType" nillable="true" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="JudgementType">
		<xs:annotation>
			<xs:documentation>
				In case of an ending of the legal cohabitation, a judgement can be associated with it (code 6)
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="date" type="dat:DateType" nillable="true"/>
			<xs:element name="number" type="num:PersonNumberType" nillable="true"/>
			<xs:element name="place" type="cod:CodeType" nillable="true"/>
			<xs:element name="tribunal" type="cod:CodeType" nillable="true"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="CessationDocumentsType">
		<xs:annotation>
			<xs:documentation>
				Container for legal-cohabitation-related documents. For documents that do not exist, the corresponding element
				is simply omitted.
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="judgement" type="leg:JudgementType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="PlaceType" abstract="true">
		<xs:annotation>
			<xs:documentation>
				Base class of PlaceTypes. 
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="municipality" type="cod:CodeType" nillable="true" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
						The municipality is defined as a code type, which holds a simple code and a description.

						For Belgian municipalities, the simple code will be the INS/NIS code, and language-dependent
						descriptions providing the name of the municipality may be added.

					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="country" type="cod:CodeType" nillable="true" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
						The country is defined as a code type, which holds a simple code and a description.
						In general, the ISO country code is used as the simple code, and language-dependent
						descriptions providing the name of the country may be added. 
					</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="PartnerBirthPlaceType">
		<xs:annotation>
			<xs:documentation>
				The birthplace of the legal cohabitation partner
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="leg:PlaceType">
				<xs:sequence>
					<!--  no additional items -->
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="CessationPlaceType">
		<xs:annotation>
			<xs:documentation>
				A legal-cohabitation-cessation place is generally defined as a municipality (NIS/INS code) or country.
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="leg:PlaceType">
				<xs:sequence>
					<!--  no additional items -->
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="LegalCohabitationPlaceType">
		<xs:annotation>
			<xs:documentation>
				A legal-cohabitation place is generally defined as a municipality (NIS/INS code) or country.
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="leg:PlaceType">
				<xs:sequence>
					<!--  no additional items -->
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="LegalCohabitationRegistrationPlaceType">
		<xs:annotation>
			<xs:documentation>
				The place where the registration officer (=notary) registered the legal cohabitation.
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="leg:PlaceType">
				<xs:sequence>
					<!--  no additional items -->
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="RegistrationOfficerType">
		<xs:annotation>
			<xs:documentation>
				A registration officer is the notary making the registration of the legal cohabitation. This registration has to be presented to the municipality where the legal cohabitation will take place.
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="registrationOfficerName" type="xs:string" nillable="true"/>
			<xs:element name="registrationPlace" type="leg:LegalCohabitationRegistrationPlaceType" nillable="true" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="LegalCohabitationCessationType">
		<xs:annotation>
			<xs:documentation>
				A legal-cohabitation-cessation contains a date of the event, the reason for cessation, the place where the statement was made, the date and place of aknowledgement by municipality/country and possibly information about the judgement associated with it.
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="registrationDate" type="dat:DateType" nillable="true"/>
			<xs:element name="reason" type="cod:CodeType" nillable="true" minOccurs="0"/>
			<xs:element name="cessationPlace" type="leg:CessationPlaceType" nillable="true" minOccurs="0"/>
			<xs:element name="acknowledgementDate" type="dat:DateType" nillable="true" minOccurs="0"/>
			<xs:element name="acknowledgementPlace" type="leg:CessationPlaceType" nillable="true" minOccurs="0"/>
			<xs:element name="cessationDocuments" type="leg:CessationDocumentsType" nillable="true" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="LegalCohabitationDeclarationType">
		<xs:annotation>
			<xs:documentation>
				A legal-cohabitation-declaration contains a date of registration, details of the legalCohabitationPartner, a place (municipality NIS code or country ISO code) and possibly information about the notary who made the registration
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="registrationDate" type="dat:DateType" nillable="true"/>
			<xs:element name="legalCohabitationPartner" type="leg:LegalCohabitationPartnerType" nillable="true" minOccurs="0"/>
			<xs:element name="legalCohabitationPlace" type="leg:LegalCohabitationPlaceType" nillable="true" minOccurs="0"/>
			<xs:element name="registrationOfficer" type="leg:RegistrationOfficerType" nillable="true" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="LegalCohabitationInformationType">
		<xs:sequence>
			<xs:element name="statementDate" type="dat:DateType" nillable="true" minOccurs="0"/>
			<xs:element name="legalCohabitationDeclaration" type="leg:LegalCohabitationDeclarationType" nillable="true" minOccurs="0"/>
			<xs:element name="legalCohabitationCessation" type="leg:LegalCohabitationCessationType" nillable="true" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
</xs:schema>
